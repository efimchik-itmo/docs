# Баллы

# ИТМО Программирование 2022 весна

| Activity                                  | Details                               | Scores | Sum     |
|-------------------------------------------|---------------------------------------|--------|---------|
| **Autocode**                              |                                       |        | **40**  |
| Java Collections +                        |                                       |        |         |
|                                           | Collections. Count Words              | 2      |         |
| Design Patterns                           |                                       |        |         |
|                                           | Iterable Table                        | 1      |         |
|                                           | Array X-times Iterators               | 1      |         |
|                                           | Card Dealing Strategy                 | 1      |         |
|                                           | Even Index Elements SubList Decorator | 1      |         |
|                                           | Repository Observer                   | 2      |         |
|                                           | Plot Factories                        | 2      |         |
| Lambdas & Streams                         |                                       |        |         |
|                                           | Streams. Count words                  | 2      |         |
|                                           | Streams. Pipelines.                   | 3      |         |
| IO                                        |                                       |        |         |
|                                           | File Tree                             | 3      |         |
| JUnit                                     |                                       |        |         |
|                                           | Test Sorting                          | 2      |         |
|                                           | Test Quadratic Equation               | 2      |         |
|                                           | Test Factorial                        | 3      |         |
| Concurrency                               |                                       |        |         |
|                                           | Thread Factoring                      | 3      |         |
|                                           | Concurrent TicTacToe                  | 3      |         |
| Java + SQL. JDBC                          |                                       |        |         |
|                                           | Employees. SQL Queries.               | 1      |         |
|                                           | Employees. Row Mapper.                | 1      |         |
|                                           | Employees. Set Mapper.                | 2      |         |
|                                           | Employees. DAO.                       | 2      |         |
|                                           | Employees. Service.                   | 3      |         |
| Algorithms and Data Structures (Optional) |                                       |        |         |
|                                           | Flood Fill                            | +1     |         |
|                                           | BST Pretty Print                      | +2     |         |
|                                           | HashTable 8-16                        | +2     |         |
| **Artifacts**                             |                                       |        | **20**  |
|                                           | Autocode Exercise                     | 10     |         |
|                                           | LitCode Contest                       | 10     |         |
| **Project**                               |                                       |        | **20**  |
|                                           | HLO                                   | 3      |         |
|                                           | DD                                    | 7      |         |
|                                           | Implementation                        | 10     |         |
| **Exam**                                  |                                       |        | **20**  |
| **Total**                                 |                                       |        | **100** |

\
\
\
\
\
\
\
\
\
\
\
\
\
