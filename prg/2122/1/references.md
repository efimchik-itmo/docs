# Список источников

*Note:* Список будет пополняться.

## Журнал с вашими результатами
Программирование 2021 Fall - https://docs.google.com/spreadsheets/d/1PxRpdHOWdU0oZjcaYTPg0kRj3fAz_Z_6yf9zh71dSlI/edit?usp=sharing

## Практика

- Класс в курсе **"Введение в программирование (C++)"**\
[https://stepik.org/join-class/8dd31694aceeac01cb9587099c991098175b2b62](https://stepik.org/join-class/8dd31694aceeac01cb9587099c991098175b2b62)
- **Курс Java Basics в Autocode**:\
[https://autocode-next.lab.epam.com/courses/invitations/j8VyvDKMq21iIGwQGttk1oTWQtxSmS3c](https://autocode-next.lab.epam.com/courses/invitations/j8VyvDKMq21iIGwQGttk1oTWQtxSmS3c)

## Рекомендуемая литература
- В. Спрингер - **Гид по Computer Science**\
Тонкая универсально полезная книжка. Рассказано об оценке сложности алгоритмов, самих алгоритмов и структур данных, а также приемах их проектирования.

- Б. Керниган, Д. Ритчи - **Язык программирования С**\
Книга по основам Си (не плюсов) и, следовательно, структурному программированию.
Про специфические для языка фишки вроде указателей и функций управления памятью тоже рассказано.

- Б. Керниган, Р. Пайк - **Практика программирования**\
Книга рассказывает о концепциях и приемах для решения задач, которые часто возникают в практике программирования.
Например, рассказывают и об оценке сложности алгоритма и, например, и о концепции интерфейсов.
Примеры даны разных языках с обзором их особенностей.

- Б. Страуструп - **Дизайн и эволюция языка С++**\
Если плюсы поселились в вашем сердце, то эта книга - для вас.
Создатель языка расскажет, какие решения в языке есть и почему так было сделано.

- К. Хорстманн - **Java. Библиотека профессионала.** 
    - Том 1. Основы
    - Том 2. Расширенные средства программирования\
Классический здоровенный дорогой двухтомник по Java.
Очень большой, подробный и мой любимый.
Описано почти все, что вам может понадобиться из Java SE.

## Дополнительные материалы

### С++ in-depth
- **Программирование на языке C++** - [https://stepik.org/course/7](https://stepik.org/course/7)\
Курс достаточно глубоко погружает в особенности программирования именно на плюсах и дает много теории,
в том числе и о реализации инструментов ООП.
Практика тоже есть, и она непростая, хотя ее и немного.

### Frontend
- Д. Дакетт - **Javascript и jQuery. Интерактивная веб-разработка**
- Д. Дакетт - **HTML и CSS. Разработка и дизайн веб-сайтов**
Это не очень большие книги по фронтенду, выделяющиеся продуманным оформлением.
Для фронтенда чувство визуального стиля, пожалуй, важно. Уточню, что книги охватывают именно основы,
так что рекомендовать их могу в основном именно новичкам.

### git
- S. Chacon, B. Straub. **Pro Git.**\
    Основная книга по Git. Распространяется бесплатно в электронном виде. Купить напечатанную версию тоже, конечно, можно. 
    - Оригинал: [https://git-scm.com/book/en/v2](https://git-scm.com/book/en/v2)
    - Перевод: [https://git-scm.com/book/ru/v2](https://git-scm.com/book/ru/v2)
- **Онлайн курс по Git от EPAM**: [https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e](https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e)
- **Онлайн курс по Git от devcolibri**: [https://devcolibri.com/course/git-для-начинающих/](https://devcolibri.com/course/git-%D0%B4%D0%BB%D1%8F-%D0%BD%D0%B0%D1%87%D0%B8%D0%BD%D0%B0%D1%8E%D1%89%D0%B8%D1%85/)
- **Вводная статья по Git от JRebel + CheatSheet**: [https://www.jrebel.com/blog/git-cheat-sheet](https://www.jrebel.com/blog/git-cheat-sheet)
- **Git CheetSheet от GitHub**:[https://training.github.com/downloads/ru/github-git-cheat-sheet/](https://training.github.com/downloads/ru/github-git-cheat-sheet/)
- **Интерактивный Git CheetSheet**: [https://ndpsoftware.com/git-cheatsheet.html](https://ndpsoftware.com/git-cheatsheet.html)
- Перевод старой известной статьи **A successful Git branching model**, описывающей Git Flow: [https://habr.com/ru/post/106912/](https://habr.com/ru/post/106912/)
- **Learning Git Branching**: [https://learngitbranching.js.org/](https://learngitbranching.js.org/) \
    Интерактивный тренажер по освоению ветвления в git

### CLion
- Бесплатные лицензии для студентов - https://www.jetbrains.com/ru-ru/community/education/#students
- Список продуктов (советую ставить Toolbox) - https://www.jetbrains.com/ru-ru/products/
- Туториал по настройке тулчейнов - https://www.jetbrains.com/help/clion/quick-tutorial-on-configuring-clion-on-windows.html
- MinGW 64 Installer - http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/installer/mingw-w64-install.exe/download
- Cygwin Home - https://cygwin.com

### Data Types (2021-09-14)
Пара статей на тему занятия
- [Ликбез по типизации в языках программирования](https://habr.com/ru/post/161205/)
- [Понимание lvalue и rvalue в C и С++](https://habr.com/ru/post/348198/)
- [Инициализация в современном C++](https://habr.com/ru/company/jugru/blog/469465/)
- [JavaScript's Memory Management Explained](https://felixgerschau.com/javascript-memory-management/)
- [Demystifying JavaScript Variable Scope and Hoisting](https://www.sitepoint.com/demystifying-javascript-variable-scope-hoisting/)


### Java Intro (2021-10-19)
Материалы для подготовки к практикуму в Автокоде:
- **Онлайн курс по Git от EPAM**\
  [https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e](https://learn.epam.com/detailsPage?id=601f195a-d408-4439-a16d-0630ed2a412e) \
  Курс по git. Нужно изучить, чтобы понять, как работать с задачами из Автокода.
- **Онлайн-курс по Maven от EPAM**\
  [https://learn.epam.com/detailsPage?id=2d408aa1-18c7-4da9-bed0-9ee229431c80](https://learn.epam.com/detailsPage?id=2d408aa1-18c7-4da9-bed0-9ee229431c80)
  Курс по Maven. Можно изучить, чтобы лучше понимать, как работать с задачами из Автокода.
- Видео от JetBrains:
  - IntelliJ IDEA. Cloning a Project from GitHub (для гитлаба тоже актуально)\
    https://www.youtube.com/watch?v=aBVOAnygcZw
  - Working With Maven in IntelliJ IDEA\
    https://www.youtube.com/watch?v=pt3uB0sd5kY
