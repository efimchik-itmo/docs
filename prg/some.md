```mermaid
classDiagram

        Iterable ..o Iterator
        Iterable <|.. Collection
        Collection <|.. List
        Collection <|.. Set
        Set <|.. SortedSet
        SortedSet <|.. NavigableSet
        Collection <|.. Queue
        Queue <|.. Deque
        
        List <|-- ArrayList
        List <|-- LinkedList
        Deque <|-- LinkedList
        Queue <|-- PriorityQueue
        NavigableSet <|-- TreeSet
        Set <|-- HashSet
                
        
        class Iterable:::interface{
            <<interface>>
            +iterator()
        }
        class Iterator {
            <<interface>>
            +hasNext()
            +next()
        }
        class Collection{
            <<interface>>
            +add()
            +remove()
            +contains()
            +size()
        }
        class List{
            <<interface>>
            +get()
            +set()
            +sort()
            +indexOf()
            +subList()
        }
        class Set{
            <<interface>>
        }
        class Queue{
            <<interface>>
            +offer()
            +peek()
            +poll()
        }
        class Deque{
            <<interface>>
            +offerFirst()
            +offerLast()
            +peekFirst()
            +peekLast()
            +pollFirst()
            +pollLast()
        }
        class SortedSet{
            <<interface>>
            +first()
            +last()
            +headSet()
            +tailSet()
        }
        class NavigableSet{
            <<interface>>
            +ceiling()
            +floow()
            +lower()
            +higher()
            +pollFirst()
            +pollLast()
        }
        
        
        class ArrayList{
        }
        class LinkedList{
        }
        class HashSet{
        }
        class TreeSet{
        }
        class PriorityQueue{
        }
        

```