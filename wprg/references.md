# Список источников

*Note:* Список будет пополняться.

### Практика
- Autocode. Java: SQL and JDBC - [https://autocode.lab.epam.com/student/group/78](https://autocode.lab.epam.com/student/group/78)
- Autocode. Java: Servlets - [https://autocode.lab.epam.com/student/group/79](https://autocode.lab.epam.com/student/group/79)
- Autocode. Java: Spring Core - [https://autocode.lab.epam.com/student/group/112](https://autocode.lab.epam.com/student/group/112)
- Autocode. Java: Spring MVC - [https://autocode.lab.epam.com/student/group/83](https://autocode.lab.epam.com/student/group/83)

### Методическое пособие
[Основы разработки ИС](https://vk.com/doc-154193295_518816775)

### Рекомендуемые материалы

#### Java
- К. Хорстманн - Java. Библиотека профессионала. 
    - Том 1. Основы
    - Том 2. Расширенные средства программирования\
Классический здоровенный дорогой двухтомник по Java. Очень большой, подробный и мой любимый. Содержит главу по JDBC.
- И. Блинов, В. Романчик - Java. Методы программирования.\
Есть главы по сервлетам и JDBC.\
Можно свободно скачать тут: [https://careers.epam.by/training/books](https://careers.epam.by/training/books). 

#### Spring
- Ю. Козмина, Р. Харроп - Spring 5 для профессионалов\
Классический толстенный том о Spring. Мне, опять же, очень нравится, но может показаться слишком большим.
- DI Motivation\
Репозиторий с примером эволюции от Hello World до проекта с внедрением зависимостей через Spring.
Именно на его основе я вам объяснял, зачем нужно внедрение зависимостей, только мы заменили тему на выборы в США.
[https://github.com/thejerome/SpringTutorialSpringCore](https://github.com/thejerome/SpringTutorialSpringCore)
- Коллекция гайдов от разработчиков Spring: [https://spring.io/guides](https://spring.io/guides)\
Очень много гайдов, некоторые из которых вам даже подойдут. Некоторые переведены на русский вот здесь: [https://spring-projects.ru/guides/](https://spring-projects.ru/guides/)
- На Baeldung многие статьи и гайды относятся к Spring: [https://www.baeldung.com/spring-tutorial](https://www.baeldung.com/spring-tutorial)
- Небольшое резюме по Spring MVC с уточненными ссылками: [https://vk.com/@efimchik_post_edu-wprg-4](https://vk.com/@efimchik_post_edu-wprg-4)

#### BD. SQL
- С. Куликов - Реляционные базы данных в примерах.\
Книга сравнительно небольшая, однако охватывает все основные аспекты реляционных БД.\
Можно свободно скачать тут: http://svyatoslav.biz/relational_databases_book_download/
- SQL Bolt.[https://sqlbolt.com/](https://sqlbolt.com/)\
Тренажер по SQL - используйте для того, чтобы разобраться в основных командах SQL
- Технострим от разработчика Аллодов из mail.ru по БД (долго, подробно): [https://www.youtube.com/watch?v=SfYaAQ9-RnE&list=PLrCZzMib1e9oOFQbuOgjKYbRUoA8zGKnj&index=1](https://www.youtube.com/watch?v=SfYaAQ9-RnE&list=PLrCZzMib1e9oOFQbuOgjKYbRUoA8zGKnj&index=1)\
Вот тут вам подробно расскажут о деталях, которые мы опустили - что такое изоляция транзакций, как устроены индексы, что такое explain и т.д.

#### UML
- М. Фаулер - UML. Основы. Краткое руководство по стандартному языку объектного моделирования\
Кратко, но основательно изложено практически все, что нужно от UML разработчику ПО в профессиональной деятельности.

#### Spring Boot
**Spring Boot** - это такой фреймворк для быстрой разработки приложений, использующий Spring Core, Spring MVC и прочие подфреймворки экосистемы Spring.\
Если коротко, то вы создаете проект Spring Boot ([start.spring.io](start.spring.io)), указав, что вам нужен веб-слой и БД, то спринг бут создаст нужные бины и настроит их сам - data source, dispatcher servlet, вот это все.
- Ф. Гутьеррес - Spring Boot 2\
Актуальная книга по Spring Boot, в которой все про него и смежные фреймворки рассказано.
- Гайд о том, как завести Spring Boot приложение.
https://spring.io/guides/gs/spring-boot/
- Более подробные многостраничные туториалы:
    - Baeldung: https://www.baeldung.com/spring-boot
    - Tutorialspoint: https://www.tutorialspoint.com/spring_boot/index.htm
- Официальное руководство:
https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/

#### JDBC Template
Это некоторая надстройка над JDBC, облегчающая работу с ним. Не требует погружения в парадигму ORM, как JPA и Hibernate, и может стать удобной основой для доустпа к БД в вашем приложении.
- Базовый гайд:
    - [https://spring.io/guides/gs/relational-data-access/](https://spring.io/guides/gs/relational-data-access/)
    - [https://spring-projects.ru/guides/relational-data-access/](https://spring-projects.ru/guides/relational-data-access/) (перевод на русский, немножко устаревшая версия)
- Еще туториалы:
    - Baeldung: https://www.baeldung.com/spring-jdbc-jdbctemplate
    - Alex Kosarev (на русском): https://alexkosarev.name/2016/06/13/spring-framework-jdbctemplate/

#### ORM (Hibernate)
**ORM** - подход настройки маппинга SQL-сущностей на Java-объекты. Используется для доступа к БД. Иногда работает как по волшебству, иногда не работает, и тоже как по волшебству. Так будет до тех пор, пока не разберетесь, как работает волшебство.

- Туториал, как настроить Hibernate В Spring (Baeldung):\
https://www.baeldung.com/hibernate-5-spring
- Туториал по Hibernate В Spring Boot (o7planning, на русском):\
https://o7planning.org/ru/11665/spring-boot-hibernate-and-spring-transaction-tutorial

#### Spring Data JPA
Этот фреймворк позволяет макисмально абстрагироваться от почти всех технических особенностей ORM и подходит для быстрой разработки.
Но нужно учитывать, что это - магия поверх другой магии, поэтому при возникновении проблем часто нужно глубоко понимать и ORM, и Spring Data.

- Базовый туториал:
    - https://spring.io/guides/gs/relational-data-access/
    - https://spring-projects.ru/guides/relational-data-access/ (перевод)
- Неплохая недлинная статья на хабре:\
https://habr.com/ru/post/435114/


#### О загрузке/отдаче файлов в Spring MVC и сохранении их в БД.
*По вопросу на занятии 18.12.20 - Как сохранять картинку в БД, а затем ее по запросу отдавать?*
- Как обработать присланный файл: https://www.baeldung.com/spring-file-upload
- Как отдать картинку в ответ на запрос: https://www.baeldung.com/spring-mvc-image-media-data
- Как работать с BLOB в SQL: https://www.ibm.com/support/knowledgecenter/ssw_ibm_i_74/rzaha/adwrblob.htm

#### Материалы для экзаменационных задач
- Туториал по JSP - https://www.tutorialspoint.com/jsp/index.htm
- Как делать Spring Custom Scope - https://www.baeldung.com/spring-custom-scope
- Как конфигурировать Spring Security - https://www.baeldung.com/spring-security-basic-authentication
