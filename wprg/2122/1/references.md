# Список источников

*Note:* Список будет пополняться.

### Электронный журнал
Веб-Программирование 2021 Fall - https://docs.google.com/spreadsheets/d/18zmxWz9lPDJGv2_k2JQtzHmaWJdEf6ngcUf0ynt5c14/edit?usp=sharing

### Практика
- Autocode. - [https://autocode.lab.epam.com/student/group/320](https://autocode.lab.epam.com/student/group/320)

### Проект
Формы для сдачи этапов:

| Этап                    | Ссылка                              |
|-------------------------|-------------------------------------|
| Заявка. HLO + Analogues | https://forms.gle/o9a3pPgtckAEcSQt8 | 
| Use Cases               | https://forms.gle/UcMva3EsLEEL3w2F7 | 
| Persistence             | https://forms.gle/kdJgSegoAfsKJWEB6 | 
| UI                      | https://forms.gle/GZwanrsSCzdsxbsG7 | 
| Finale                  | https://forms.gle/nXFJMHRamDb9EtVA7 | 

### Методическое пособие
[Основы разработки ИС](https://vk.com/doc-154193295_518816775)

### Рекомендуемые материалы

### К занятию JDBC Extra (2021 09 10)
- The **DAO Pattern** in Java, *Baeldung*\
  https://www.baeldung.com/java-dao-pattern
- **DAO vs Repository** Patterns, *Baeldung*\
  https://www.baeldung.com/java-dao-vs-repository
- **SQL Injection** Prevention Cheat Sheet, *OWASP*\
  https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html
- Репозиторий для **Демо**\
  https://gitlab.com/efimchik-itmo/web-prg/demo-jdbc-extra

### К занятию HTTP & Servlets:
- Репозиторий для **Демо**\
  https://github.com/thejerome/servlets-demo

### К занятию Project Design Steps:
- Проектирование use case диаграммы. Определение функциональных возможностей системы.\
  WorldSkills Russia\
  Полноценный онлайн-урок по составлению диаграммы прецедентов использования\
  https://nationalteam.worldskills.ru/skills/proektirovanie-use-case-diagrammy-opredelenie-funktsionalnykh-vozmozhnostey-sistemy/
- UML State Diagrams, Dr. Dana\
  Хороший ролик про диаграммы состояний\
  https://www.youtube.com/watch?v=5at5CmOAnkM

### К занятию Spring Intro:
  - Репозиторий для **Демо**\
    https://github.com/thejerome/SpringTutorialSpringCore

### К занятию Spring AOP:
  - Репозиторий для **Демо**\
    https://github.com/thejerome/spring-aop-demo

### К занятию Spring MVC:
  - Репозиторий для **Демо**\
    https://github.com/thejerome/spring-mvc-itmo-demo

### К занятию Spring Persistence:
  - Репозитории для **Демо**\
    - https://github.com/thejerome/demo-jdbc-template
    - https://github.com/thejerome/demo-spring-jpa
    - https://github.com/thejerome/demo-spring-data-jpa

### К занятию Spring Boot:
  - Репозитории для **Демо**\
    https://github.com/thejerome/spring-boot-demo-itmo

### К занятию Spring Security:
  - Репозитории для **Демо**\
    - https://github.com/thejerome/demo-spring-security
    - https://github.com/thejerome/spring-boot-security-demo

#### Java
- К. Хорстманн - Java. Библиотека профессионала. 
    - Том 1. Основы
    - Том 2. Расширенные средства программирования\
Классический здоровенный дорогой двухтомник по Java. Очень большой, подробный и мой любимый. Содержит главу по JDBC.
- И. Блинов, В. Романчик - Java. Методы программирования.\
Есть главы по сервлетам и JDBC.\
Можно свободно скачать тут: [https://careers.epam.by/training/books](https://careers.epam.by/training/books).
- Туториал по JSP - https://www.tutorialspoint.com/jsp/index.htm

#### Spring
- Ю. Козмина, Р. Харроп - Spring 5 для профессионалов\
Классический толстенный том о Spring. Мне, опять же, очень нравится, но может показаться слишком большим.
- Коллекция гайдов от разработчиков Spring: [https://spring.io/guides](https://spring.io/guides)\
Очень много гайдов, некоторые из которых вам даже подойдут. Некоторые переведены на русский вот здесь: [https://spring-projects.ru/guides/](https://spring-projects.ru/guides/)
- На Baeldung многие статьи и гайды относятся к Spring: [https://www.baeldung.com/spring-tutorial](https://www.baeldung.com/spring-tutorial)
- Небольшое резюме по Spring MVC с уточненными ссылками: [https://vk.com/@efimchik_post_edu-wprg-4](https://vk.com/@efimchik_post_edu-wprg-4)
- Как делать Spring Custom Scope - https://www.baeldung.com/spring-custom-scope
- Как конфигурировать Spring Security - https://www.baeldung.com/spring-security-basic-authentication

#### BD. SQL
- С. Куликов - Реляционные базы данных в примерах.\
Книга сравнительно небольшая, однако охватывает все основные аспекты реляционных БД.\
Можно свободно скачать тут: https://careers.epam.by/training/books
- Relational Databases Basics. Курс от EPAM, от С. Куликова.
  - Agenda
    - Databases Fundamentals
    - Relations, Keys, Relationships, Indexes
    - Normalization and Normal Forms
    - Database Modelling
    - Additional Database Objects and Processes
    - Database Quality
  - На русском: https://epa.ms/rdb-rus
  - На английском: https://epa.ms/rdb-eng
- SQL by Examples. Курс от EPAM, от С. Куликова.
  - Agenda
    - Introduction and Databases Overview
    - Queries to Select and Modify Data
    - Views
    - Triggers
    - Stored Functions and Stored Procedures
    - Transactions
    - Typical Operations
  - На русском: https://epa.ms/sql-rus
  - На английском: https://epa.ms/sql-eng
- SQL Bolt. https://sqlbolt.com \
Тренажер по SQL - используйте для того, чтобы разобраться в основных командах SQL
- Технострим от разработчика Аллодов из mail.ru по БД (долго, подробно): https://www.youtube.com/watch?v=SfYaAQ9-RnE&list=PLrCZzMib1e9oOFQbuOgjKYbRUoA8zGKnj&index=1 \
Вот тут вам подробно расскажут о деталях, которые мы опустили - что такое изоляция транзакций, как устроены индексы, что такое explain и т.д.

#### UML
- М. Фаулер - UML. Основы. Краткое руководство по стандартному языку объектного моделирования\
Кратко, но основательно изложено практически все, что нужно от UML разработчику ПО в профессиональной деятельности.

#### Spring Boot
**Spring Boot** - это такой фреймворк для быстрой разработки приложений, использующий Spring Core, Spring MVC и прочие подфреймворки экосистемы Spring.\
Если коротко, то вы создаете проект Spring Boot ([start.spring.io](start.spring.io)), указав, что вам нужен веб-слой и БД, то спринг бут создаст нужные бины и настроит их сам - data source, dispatcher servlet, вот это все.
- Ф. Гутьеррес - Spring Boot 2\
Актуальная книга по Spring Boot, в которой все про него и смежные фреймворки рассказано.
- Гайд о том, как завести Spring Boot приложение.
https://spring.io/guides/gs/spring-boot/
- Более подробные многостраничные туториалы:
    - Baeldung: https://www.baeldung.com/spring-boot
    - Tutorialspoint: https://www.tutorialspoint.com/spring_boot/index.htm
- Официальное руководство:
https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/

#### JDBC Template
Это некоторая надстройка над JDBC, облегчающая работу с ним. Не требует погружения в парадигму ORM, как JPA и Hibernate, и может стать удобной основой для доустпа к БД в вашем приложении.
- Базовый гайд:
    - [https://spring.io/guides/gs/relational-data-access/](https://spring.io/guides/gs/relational-data-access/)
    - [https://spring-projects.ru/guides/relational-data-access/](https://spring-projects.ru/guides/relational-data-access/) (перевод на русский, немножко устаревшая версия)
- Еще туториалы:
    - Baeldung: https://www.baeldung.com/spring-jdbc-jdbctemplate
    - Alex Kosarev (на русском): https://alexkosarev.name/2016/06/13/spring-framework-jdbctemplate/

#### ORM (Hibernate)
**ORM** - подход настройки маппинга SQL-сущностей на Java-объекты. Используется для доступа к БД. Иногда работает как по волшебству, иногда не работает, и тоже как по волшебству. Так будет до тех пор, пока не разберетесь, как работает волшебство.

- Туториал, как настроить Hibernate В Spring (Baeldung):\
https://www.baeldung.com/hibernate-5-spring
- Туториал по Hibernate В Spring Boot (o7planning, на русском):\
https://o7planning.org/ru/11665/spring-boot-hibernate-and-spring-transaction-tutorial

#### Spring Data JPA
Этот фреймворк позволяет макисмально абстрагироваться от почти всех технических особенностей ORM и подходит для быстрой разработки.
Но нужно учитывать, что это - магия поверх другой магии, поэтому при возникновении проблем часто нужно глубоко понимать и ORM, и Spring Data.
- Базовый туториал:
    - https://spring.io/guides/gs/relational-data-access/
    - https://spring-projects.ru/guides/relational-data-access/ (перевод)
- Неплохая недлинная статья на хабре:\
https://habr.com/ru/post/435114/
