## Список вопросов к экзамену

1. Веб-приложения. Что такое веб-приложение, чем оно отличается от обычного приложения?
2. Веб-приложения. Что такое MVC? Для чего применяется?
3. Веб-приложения. Что такое слоистая архитектура? Для чего применяется?
4. HTTP. Что это, зачем используется, какие методы существуют, для чего применяются?
5. HTTP. Какой формат запросов и ответов.
6. Servlet. Что это, зачем применяется, какой API и жизненный цикл?
7. Servlet. Что такое контейнер сервлетов, как он работает с сервлетами? Какие бывают?
8. Servlet. Что такое JSP? Зачем применяется, как работает?
9. Servlet. Как в сервлете извлечь данные из HTTP-запроса?
10. Servlet. Как в сервлете сформировать ответ на HTTP-запрос?
11. JDBC. Опишите основные классы и интерфейсы JDBC, их назначение.
12. JDBC. Опишите, какие бывают виды Statement, в чем их особенности.
13. JDBC. Опишите, как с помощью JDBC объединить несколько выражений в одну транзакцию.
14. JDBC. Что такое SQL-инъекция? Как от нее защититься?
15. Spring DI. Что такое Dependency Injection?
16. Spring DI. Что такое Bean? Откуда они берутся?
17. Spring DI. Что такое Bean Scope? Какие бывают, в чем отличия?
18. Spring DI. Что такое Bean Lifecycle? Какие в нем есть этапы?
19. Spring DI. Что такое PostProcessor? Какие бывают?
20. Spring DI. Что такое Application Context? Какие виды бывают?
21. Spring DI. Что такое конфигурация в Spring? Какие виды бывают?
22. Spring AOP. Что такое AOP? Для чего нужен? Какие в нем основные термины?
23. Spring AOP. Что такое JoinPoint? Какие JoinPoint использует Spring?
24. Spring AOP. Что такое Pointcut? Какие Pointcut использует Spring?
25. Spring AOP. Что такое Advice? Какие Advice использует Spring?
26. Spring MVC. Как работает Spring MVC?
27. Spring MVC. Как работает Request Mapping в Spring MVC? Какие есть варианты?
28. Spring MVC. Какие основные параметры могут принимать handler-методы контроллеров?
29. Spring MVC. Как работает View Resolving в Spring MVC? Какие есть варианты?
30. Spring MVC. Как заставить Spring MVC преобразовывать POJO в JSON?
31. Spring Persistence. JdbcTemplate. Что это, как его можно использовать?
32. Spring Persistence. ORM. Что это, как это поддерживается в Java?
33. Spring Persistence. JPA. Что это, как это используется?
34. Spring Persistence. JPA. Как настроить JPA в Spring?
35. Spring Persistence. Spring Data JPA. Что это такое, зачем нужно, как настроить и использовать?
36. Spring Boot. Что это, зачем нужно?
37. Spring Boot. Как работает автоконфигурация? Что это, зачем нужно?
38. Spring Boot. Как Spring Boot преднастраивает слой MVC?
39. Spring Boot. Как Spring Boot преднастраивает слой взаимодействия с БД?
40. Spring Security. Что это, зачем нужно?
41. Spring Security. Как Spring Security работает в SpringMVC-приложениях?
42. Spring Security. Что такое HttpSecurity в Spring Security, как его настроить?
43. Spring Security. Что такое AuthenticationManagerBuilder в Spring Security, как его настроить?